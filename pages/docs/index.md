---
hide:
  - footer
  - toc
---

# UCL PBa IT Sikkerhed - _fagtitel_

På dette website finder du ugeplaner, øvelser, dokumenter og links til brug i faget  


## _Fagtitel_ _forår/efterår_ _YYYY_

Fagområdet indeholder ...

## Læringsmål fra studie ordningen

### Viden

### Færdigheder

### Kompetencer

![Image light theme](images/UCL_horisontal_logo_UK_rgb.png#only-light){ width="300", align=center }  
![Image dark theme](images/UCL_horisontal_logo_UK_neg_rgb.png#only-dark){ width="300", align=right }  