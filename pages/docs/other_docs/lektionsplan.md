---
hide:
  - footer
---

# Introduktion

Fagområdet indeholder ...

Faget er på XX ECTS point.

# Læringsmål

**Viden**

Den studerende har ...

- ..
- ..


**Færdigheder**

Den studerende kan ...

- ..
- ..

**Kompetencer**

Den studerende kan ...

- ..
- ..

Se [Studieordningen sektion x.x](https://esdhweb.ucl.dk/D21-1632339.pdf)

# Lektionsplan

Se ITSLearning

# Studieaktivitets modellen

![study activity model](Study_Activity_Model.png)

## Andet

Intet på nuværende tidspunkt
